<?php namespace Settings;

use Clearweb\Clearwebapps\Setting\SettingPackage;

class BittrexSettings extends SettingPackage
{
    public function __construct()
    {
        $this->setName('bittrex');
        $this->setTitle('Bittrex settings');
        $this->addWidget(new BittrexCredentialsWidget);
    }
}