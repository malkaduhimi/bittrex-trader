<?php namespace Settings;

use Clearweb\Clearwebapps\Widget\SettingWidget;
use Clearweb\Clearwebapps\Form;

class BittrexCredentialsWidget extends SettingWidget
{
    public function init()
    {
        $this->addSetting('key')
            ->addSetting('secret')
            ->setName('bittrex creds widget')
            ->setTitle('Bittrex credentials')
            ;
        
        parent::init();

        $this->setForm(
                       (new Form\Form)
                       ->addField((new Form\TextField)->setName('key'))
                       ->addField((new Form\TextField)->setName('secret'))
                       ->addField((new Form\SubmitField)->setName('submit')->setLabel(trans('clearwebapps::form_widget.submit')))
                       );
        
        return $this;
    }
}