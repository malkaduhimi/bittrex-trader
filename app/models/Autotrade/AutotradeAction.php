<?php namespace Autotrade;

class AutotradeAction extends \Eloquent
{
    const STATUS_START     = 'start';
    const STATUS_BOUGHT    = 'bought';
    const STATUS_SOLD      = 'sold';
    const STATUS_CANCELLED = 'cancelled';
    
    protected $fillable = [ 'currency', 'orderId', 'amount', 'buyPrice', 'sellPrice', 'status'];

}