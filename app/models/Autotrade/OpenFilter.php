<?php namespace Autotrade;

use Clearweb\Clearworks\Communication\ParameterChanger;
use Clearweb\Clearwebapps\Action\ScriptActionButton;

class OpenFilter extends ScriptActionButton
{
    public function init()
    {
        if ($this->getParameter('only-open', false)) {
            $this->setScriptAction((new ParameterChanger)->setParameter('only-open', 0));
            $this->addClass('active');
        } else {
            $this->setScriptAction((new ParameterChanger)->setParameter('only-open', 1));
        }
        
        $this->setTitle('Show only open');
        
        return parent::init();
    }
}