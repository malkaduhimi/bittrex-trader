<?php namespace Autotrade;

use Clearweb\Clearwebapps\Page\Page;

use Clearweb\Clearwebapps\Layout\FullWidthLayout;

class AutotradePage extends Page
{
    public function __construct()
    {
        $this->setSlug('auto-trades');
    }

    public function init()
    {
        $this->setLayout(new FullWidthLayout)
            ->setTitle('Autotrades')
            ->addWidgetLinear(new AutotradeStatsWidget, 'content')
            ->addWidgetLinear(new AutotradeListWidget, 'content')
            ;
        
        parent::init();
    }
}