<?php namespace Autotrade;

use Clearweb\Clearwebapps\Widget\ViewWidget;
use App;
use Market;
use GuzzleHttp\Client;
use Cache;

class AutotradeStatsWidget extends ViewWidget
{
    private $totalProfit;
    private $profitLastDay;
    private $lastDayProfitPercent;
    
    private $btcBalance;
    
    private $openTrades;
    private $openTradeTotal;
    
    private $total;

    private $currencyMarkets;
    
    public function init()
    {
        $this->setViewName('autotrade-stats')
            ->setName('auto trade stats')
            ;
        
        $this->calcStats();
        
        $this->with('btcBalance', $this->btcBalance)
            ->with('totalProfit', $this->totalProfit)
            ->with('lastDayProfitPercent', $this->lastDayProfitPercent)
            ->with('profitLastDay', $this->profitLastDay)
            ->with('openTrades', $this->openTrades)
            ->with('openTradeTotal', $this->openTradeTotal)
            ->with('total', $this->total)
            ->with('btcToEuroPrice', $this->getBTCToEuro(1))
            ;
        
        return parent::init();
    }
    
    private function calcStats()
    {
        $this->btcBalance    = $this->getBtcBalance();
        $this->totalProfit   = 0;
        $this->profitLastDay = 0;
        $this->openTrades    = 0;
        
        foreach(AutotradeAction::where('status', AutotradeAction::STATUS_BOUGHT)->get() as $autoTrade) {
            $price = $this->getLastPrice($autoTrade->currency);
            
            $this->openTradeTotal += $autoTrade->amount * $price;
            $this->totalProfit += ($price - $autoTrade->buyPrice) * $autoTrade->amount;
            $this->openTrades++;
        }
        
        $this->profitLastDay = $this->totalProfit;
        
        $this->profitLastDay += AutotradeAction::where('status', AutotradeAction::STATUS_SOLD)
            ->where('updated_at', '>=', new \DateTime('-24 hours'))
            ->sum('profit')
            ;
        
        $profitYesterday = AutotradeAction::where('status', AutotradeAction::STATUS_SOLD)
            ->where('updated_at', '<', new \DateTime('-24 hours'))
            ->where('updated_at', '>=', new \DateTime('-48 hours'))
            ->sum('profit')
            ;
        
        $this->totalProfit += AutotradeAction::where('status', AutotradeAction::STATUS_SOLD)->sum('profit');
        
        $this->lastDayProfitPercent = $profitYesterday != 0 ? (($this->profitLastDay - $profitYesterday) / $profitYesterday * 100) : 0;
        
        $this->total = $this->btcBalance + $this->openTradeTotal;
    }
    
    private function getBtcBalance()
    {
        return Cache::get('btcBalance');
    }
    
    private function getLastPrice($currency)
    {
        $market = $this->getMarketByCurrency($currency);
        return $market->price;
    }
    
    private function getMarketByCurrency($currency)
    {
        if (isset($this->currencyMarkets[$currency]))
            return $this->currencyMarkets[$currency];
        else
            return Market::where('currency', $currency)->first();
    }
    
    private function getBTCToEuro($btc)
    {
        return Cache::get('btcToEuro') * $btc;
    }
}