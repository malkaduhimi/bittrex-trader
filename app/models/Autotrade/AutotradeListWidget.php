<?php namespace Autotrade;

use Clearweb\Clearwebapps\Eloquent\ListWidget;

use Illuminate\Database\Eloquent\Builder;
use Market;
use Settings;
use Bittrex\Bittrex;
use App;


class AutotradeListWidget extends ListWidget
{

    public function init()
    {
        $this->setModelClass('Autotrade\AutotradeAction');
        parent::init();

        $this->addAction('open-trades', new OpenFilter);
                
        $this->getList()
            ->removeColumn('buyOrderId')
            ->removeColumn('sellOrderId')
            ;
        
        if ($this->getParameter('only-open', 0) == 1) {
            $this->getList()
                ->addColumn('currentPrice', function($row) {
                        $price = $this->getLastPrice($row['currency']);
                        if ($price != 0) {
                            return number_format($price, 12);
                        } else {
                            return '-';
                        }
                    })
                ->replaceColumn('profit', function($row) {
                        return number_format(($this->getLastPrice($row['currency']) - $row['buyPrice']) * $row['amount'], 12);
                    })
                ;
        }
        
        return $this;
    }

    private function getLastPrice($currency)
    {
        $market = $this->getMarketByCurrency($currency);
        return $market->price;
    }
    
    private function getMarketByCurrency($currency)
    {
        if (isset($this->currencyMarkets[$currency]))
            return $this->currencyMarkets[$currency];
        else
            return Market::where('currency', $currency)->first();
    }
    
    protected function applyQueryFilters(Builder $builder) {
        if ($this->getParameter('only-open', 0) == 1) {
            $builder->where('status', '!=', AutotradeAction::STATUS_SOLD)->where('status', '!=', AutotradeAction::STATUS_CANCELLED);
        }
        
        return parent::applyQueryFilters($builder);
    }
}