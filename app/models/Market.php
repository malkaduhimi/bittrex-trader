<?php

class Market extends Eloquent
{
    protected $fillable = ['name', 'currency', 'active', 'autoTrade', 'price'];
}