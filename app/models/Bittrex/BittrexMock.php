<?php namespace Bittrex;

class BittrexMock extends Bittrex
{
    
    public function buyLimit($market, $quantity, $rate) 
    {
        return [
                'success' => true,
                'result' => ['uuid' => 'test']
                ];
    }
    
    public function sellLimit($market, $quantity, $rate) 
    {
        return [
                'success' => true,
                'result' => ['uuid' => 'test']
                ];
    }

    public function cancel($uuid) 
    {
        return [
                'success' => true,
                
                ];
    }

    public function getOrder($uuid)
    {
        return [
                'success' => true,
                'result' => ['IsOpen' => false]
                ];
    }
    
}