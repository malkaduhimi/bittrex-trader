<?php namespace Bittrex;

interface IBittrexAPI
{
    /**
     * Used to get the open and available trading markets at Bittrex along with other meta data.
     *
     * @return array
     */
    public function getMarkets();

    /**
     * Used to get all supported currencies at Bittrex along with other meta data.
     *
     * @return array
     */
    public function getCurrencies();

    /**
     * Used to get the current tick values for a market.
     *
     * @param string $market
     *
     * @return array
     */
    public function getTicker($market);

    /**
     * Used to get the last 24 hour summary of all active exchanges.
     *
     * @return array
     */
    public function getMarketSummaries();

    /**
     * Used to get the last 24 hour summary of all active exchanges.
     *
     * @param string $market
     *
     * @return array
     */
    public function getMarketSummary($market);
    
    /**
     * Used to get retrieve the orderbook for a given market.
     *
     * @param string  $market
     * @param string  $type
     * @param int|int $depth
     *
     * @return array
     */
    public function getOrderBook($market, $type, $depth = 20);

    /**
     * Used to retrieve the latest trades that have occured for a specific market.
     *
     * @param string $market
     *
     * @return array
     */
    public function getMarketHistory($market);

    /**
     * Used to place a buy order in a specific market.
     *
     * @param string $market
     * @param int    $quantity
     * @param float  $rate
     *
     * @return array
     */
    public function buyLimit($market, $quantity, $rate);

    /**
     * Used to place an sell order in a specific market.
     *
     * @param string $market
     * @param int    $quantity
     * @param float  $rate
     *
     * @return array
     */
    public function sellLimit($market, $quantity, $rate);

    /**
     * Used to cancel a buy or sell order.
     *
     * @param string $uuid
     *
     * @return array
     */
    public function cancel($uuid);

    /**
     * Get all orders that you currently have opened.
     *
     * @param string|null $market
     *
     * @return array
     */
    public function getOpenOrders($market = null);

    /**
     * Used to retrieve all balances from your account.
     *
     * @return array
     */
    public function getBalances();

    /**
     * Used to retrieve the balance from your account for a specific currency.
     *
     * @param string $currency
     *
     * @return array
     */
    public function getBalance($currency);

    /**
     * Used to retrieve or generate an address for a specific currency.
     *
     * @param string $currency
     *
     * @return array
     */
    public function getDepositAddress($currency);

    /**
     * Used to withdraw funds from your account.
     *
     * @param string      $currency
     * @param float       $quantity
     * @param string      $address
     * @param string|null $paymentid
     *
     * @return array
     */
    public function withdraw($currency, $quantity, $address, $paymentid = null);

    /**
     * Used to retrieve a single order by uuid.
     *
     * @param string $uuid
     *
     * @return array
     */
    public function getOrder($uuid);

    /**
     * Used to retrieve your order history.
     *
     * @param string|null $market
     *
     * @return array
     */
    public function getOrderHistory($market = null);

    /**
     * Used to retrieve your withdrawal history.
     *
     * @param string $currency
     *
     * @return array
     */
    public function getWithdrawalHistory($currency);

}