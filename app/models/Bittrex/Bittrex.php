<?php namespace Bittrex;

use GuzzleHttp\Client;

/*
 * This file is part of Bittrex PHP Client.
 *
 * (c) Brian Faust <hello@brianfaust.me>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use BrianFaust\Http\Http;

class Bittrex implements IBittrexAPI
{
    /**
     * The API key used for authentication.
     *
     * @var string
     */
    private $key;

    /**
     * The API secret used for authentication.
     *
     * @var string
     */
    private $secret;

    /**
     * Create a new Bittrex instance.
     *
     * @param string $key
     * @param string $secret
     */
    public function __construct($key, $secret)
    {
        $this->key = $key;
        $this->secret = $secret;
    }

    /**
     * Used to get the open and available trading markets at Bittrex along with other meta data.
     *
     * @return array
     */
    public function getMarkets() 
    {
        return $this->send('public/getmarkets');
    }

    /**
     * Used to get all supported currencies at Bittrex along with other meta data.
     *
     * @return array
     */
    public function getCurrencies() 
    {
        return $this->send('public/getcurrencies');
    }

    /**
     * Used to get the current tick values for a market.
     *
     * @param string $market
     *
     * @return array
     */
    public function getTicker($market) 
    {
        return $this->send('public/getticker', compact('market'));
    }

    /**
     * Used to get the last 24 hour summary of all active exchanges.
     *
     * @return array
     */
    public function getMarketSummaries() 
    {
        return $this->send('public/getmarketsummaries');
    }

    /**
     * Used to get the last 24 hour summary of all active exchanges.
     *
     * @param string $market
     *
     * @return array
     */
    public function getMarketSummary($market) 
    {
        return $this->send('public/getmarketsummary', compact('market'));
    }

    /**
     * Used to get retrieve the orderbook for a given market.
     *
     * @param string  $market
     * @param string  $type
     * @param int|int $depth
     *
     * @return array
     */
    public function getOrderBook($market, $type, $depth = 20) 
    {
        return $this->send('public/getorderbook', compact('market', 'type', 'depth'));
    }

    /**
     * Used to retrieve the latest trades that have occured for a specific market.
     *
     * @param string $market
     *
     * @return array
     */
    public function getMarketHistory($market)
    {
        return $this->send('public/getmarkethistory', compact('market'));
    }

    /**
     * Used to place a buy order in a specific market.
     *
     * @param string $market
     * @param int    $quantity
     * @param float  $rate
     *
     * @return array
     */
    public function buyLimit($market, $quantity, $rate) 
    {
        return $this->send('market/buylimit', compact('market', 'quantity', 'rate'));
    }

    /**
     * Used to place an sell order in a specific market.
     *
     * @param string $market
     * @param int    $quantity
     * @param float  $rate
     *
     * @return array
     */
    public function sellLimit($market, $quantity, $rate) 
    {
        return $this->send('market/selllimit', compact('market', 'quantity', 'rate'));
    }

    /**
     * Used to cancel a buy or sell order.
     *
     * @param string $uuid
     *
     * @return array
     */
    public function cancel($uuid) 
    {
        return $this->send('market/cancel', compact('uuid'));
    }

    /**
     * Get all orders that you currently have opened.
     *
     * @param string|null $market
     *
     * @return array
     */
    public function getOpenOrders($market = null) 
    {
        return $this->send('market/getopenorders', compact('market'));
    }

    /**
     * Used to retrieve all balances from your account.
     *
     * @return array
     */
    public function getBalances() 
    {
        return $this->send('account/getbalances');
    }

    /**
     * Used to retrieve the balance from your account for a specific currency.
     *
     * @param string $currency
     *
     * @return array
     */
    public function getBalance($currency) 
    {
        return $this->send('account/getbalance', compact('currency'));
    }

    /**
     * Used to retrieve or generate an address for a specific currency.
     *
     * @param string $currency
     *
     * @return array
     */
    public function getDepositAddress($currency) 
    {
        return $this->send('account/getdepositaddress', compact('currency'));
    }

    /**
     * Used to withdraw funds from your account.
     *
     * @param string      $currency
     * @param float       $quantity
     * @param string      $address
     * @param string|null $paymentid
     *
     * @return array
     */
    public function withdraw($currency, $quantity, $address, $paymentid = null) 
    {
        return $this->send('account/withdraw', compact('currency', 'quantity', 'address', 'paymentid'));
    }

    /**
     * Used to retrieve a single order by uuid.
     *
     * @param string $uuid
     *
     * @return array
     */
    public function getOrder($uuid) 
    {
        return $this->send('account/getorder', compact('uuid'));
    }

    /**
     * Used to retrieve your order history.
     *
     * @param string|null $market
     *
     * @return array
     */
    public function getOrderHistory($market = null) 
    {
        return $this->send('account/getorderhistory', compact('market'));
    }

    /**
     * Used to retrieve your withdrawal history.
     *
     * @param string $currency
     *
     * @return array
     */
    public function getWithdrawalHistory($currency) 
    {
        return $this->send('account/getwithdrawalhistory');
    }

    /**
     * Create and send an HTTP request.
     *
     * @param string $path
     * @param array  $arguments
     *
     * @return array
     */
    private function send($path, array $arguments = []) 
    {
        $nonce = rand();
        $uri = "https://bittrex.com/api/v1.1/{$path}?apikey={$this->key}&nonce={$nonce}";
        
        $client = new Client([
                              'headers' => [
                                            'apisign' => hash_hmac('sha512', $uri, $this->secret),
                                            'Content-Type' => 'application/json',
                                            'Cache-Control' => 'no-cache'
                                            ]
                              ]);
        
        $response = $client->post($uri, ['body' => json_encode(array_filter($arguments))]);

        return json_decode($response->getBody()->getContents(), true);
    }
}