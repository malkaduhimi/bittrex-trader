<?php namespace Statistics;

use Market;
use App;

use GuzzleHttp\Client;

use Autotrade\AutotradeAction;

class StatisticsUpdater
{
    private $bittrex;
    
    public function __construct()
    {
        $this->bittrex = App::make('bittrex');
    }
    
    public function updateMarketPrices()
    {
        $success = false;
        
        $summaryResponse = $this->bittrex->getMarketSummaries();
        if ( ! empty($summaryResponse['success'])) {
            
            foreach($summaryResponse['result'] as $marketInfo) {
                $market = Market::where('name', $marketInfo['MarketName'])->first();
                if ($market != null) {
                    $market->price = $marketInfo['Last'];
                    $market->save();
                }
            }
            
            $success = true;
        }
        
        return $success;
    }
    
    public function updateCachedValues()
    {
        $this->updateBtcToEuro();
        $this->updateBtcBalance();
    }
    
    private function updateBtcToEuro()
    {
        $client = new Client([]);
        $response = $client->post('https://blockchain.info/nl/ticker');
        $json = json_decode($response->getBody()->getContents(), true);
        Cache::put('btcToEuro', $json['EUR']['last'], 5);
    }
    
    private function updateBtcBalance()
    {
        Cache::put('btcBalance', $this->bittrex->getBalance('BTC')['result']['Balance'], 5);
    }
    
    public function updateAutotradeStatus()
    {
        // backwards compatibility
        $this->updateOldTrades();
    }
    
    // backwards compatibility
    private function updateOldTrades()
    {
        $oldAutoTrades = AutotradeAction::where('status', AutotradeAction::STATUS_SOLD)
            ->where('profit', '0.000000000000')
            ->get();
        
        foreach($oldAutoTrades as $oldAutoTrade) {
            $commission  = 0.0025 * $oldAutoTrade->sellPrice * $oldAutoTrade->amount;
            $commission += 0.0025 * $oldAutoTrade->buyPrice * $oldAutoTrade->amount;
            
            $oldAutoTrade->profit = ($oldAutoTrade->sellPrice - $oldAutoTrade->buyPrice) * $oldAutoTrade->amount - $commission;
            $oldAutoTrade->save();
        }
    }
    
    private function getLastPrice($market)
    {
        if ( ! empty($this->currentPriceCache[$market->currency])) {
            return $this->currentPriceCache[$market->currency];
        } else {
            $ticker = $this->bittrex->getTicker($market->name);
            $price  = 0;
            
            if ( ! empty($ticker['success']) && $ticker['result']['Last'] < 1) {
                $price = $ticker['result']['Last'];
                $this->currentPriceCache[$currency] = $price;
            }
            
            return $price;
        }
    }
}