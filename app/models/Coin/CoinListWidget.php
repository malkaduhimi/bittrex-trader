<?php namespace Coin;

use Clearweb\Clearwebapps\Eloquent\ListWidget;

class CoinListWidget extends ListWidget
{
    public function init()
    {
        $this->setModelClass('Market');
        parent::init();
        
        $this->getList()
            ->removeColumn('active')
            ->replaceColumn('autoTrade', function($row) {
                    return $row['autoTrade']? 'yes' : 'no';
            })
            ;
        
        
        return $this;
    }
}