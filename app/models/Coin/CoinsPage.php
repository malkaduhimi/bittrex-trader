<?php namespace Coin;

use Clearweb\Clearwebapps\Page\Page;

use Clearweb\Clearwebapps\Layout\FullWidthLayout;

class CoinsPage extends Page
{
    public function __construct()
    {
        $this->setSlug('coins');
    }

    public function init()
    {
        $this->setLayout(new FullWidthLayout)
            ->setTitle('Coins')
            ->addWidgetLinear(new CoinListWidget, 'content')
            ;

        parent::init();
    }
}