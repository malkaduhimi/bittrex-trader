<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkets extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('markets', function($t) {
            $t->increments('id');
            $t->string('name');
            $t->string('currency')->index();
            $t->boolean('active');
            $t->boolean('autoTrade')->default(true);

            $t->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('markets');
	}

}
