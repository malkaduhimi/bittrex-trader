<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutotradeActionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('autotrade_actions', function($t) {
            $t->increments('id');
            
            $t->string('currency')->index();
            
            $t->string('buyOrderId');
            $t->string('sellOrderId');
            
            $t->decimal('amount', 20, 10);
            $t->decimal('buyPrice', 17, 12);
            $t->decimal('sellPrice', 17, 12);
            
            $t->string('status')->index();
            
            $t->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('autotrade_actions');
	}

}
