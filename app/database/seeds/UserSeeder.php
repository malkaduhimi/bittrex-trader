<?php

use Clearweb\Clearwebapps\User\User;
use Clearweb\Clearwebapps\User\AssociatedRole;

class UserSeeder extends Seeder {
    public function run()
    {
        DB::table('users')->delete();

        $user = User::create(['email' => 'info@clearweb.nl', 'name'=>'Clear Web', 'password'=>\Hash::make('123456')]);
        
        AssociatedRole::create(['user_id'=>$user->id, 'role'=>'admin-login']);
    }
}