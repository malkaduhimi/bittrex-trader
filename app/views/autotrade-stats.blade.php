<div class="row">
    <div class="col-lg-3"> 
        <div class="ibox">
            <div class="ibox-content">
                <h5>BTC balance</h5>
                <h1 class="no-margins">{{number_format($btcBalance, 8)}}</h1>
                <!-- <div class="stat-percent font-bold text-navy">50<i class="fa fa-eur"></i></div> -->
                <small>&euro; {{number_format($btcBalance * $btcToEuroPrice, 2)}}</small>
	    </div>
	</div>
    </div>
    <div class="col-lg-3">
        <div class="ibox">
            <div class="ibox-content">
                <h5>Profit last day</h5>
                <h1 class="no-margins">{{number_format($profitLastDay, 8)}}</h1>
                <div class="stat-percent font-bold text-{{$lastDayProfitPercent > 0 ? 'navy' : 'danger'}}">{{number_format($lastDayProfitPercent, 2)}}% <i class="fa fa-bolt"></i></div>
		<small>&euro; {{number_format($profitLastDay * $btcToEuroPrice, 2)}}</small>
	    </div>
	</div>
    </div>
    <div class="col-lg-3">
        <div class="ibox">
	    <div class="ibox-content">
                <h5>Total profit</h5>
                <h1 class="no-margins">{{number_format($totalProfit, 8)}}</h1>
                <!--<div class="stat-percent font-bold text-danger">12% <i class="fa fa-level-down"></i></div>-->
                <small>&euro; {{number_format($totalProfit * $btcToEuroPrice, 2)}}</small>
	    </div>
	</div>
    </div>
    <div class="col-lg-3">
        <div class="ibox">
            <div class="ibox-content">
                <h5>Total status</h5>
                <h1 class="no-margins">{{number_format($total, 8)}}</h1>
                <!-- <div class="stat-percent font-bold text-danger">24% <i class="fa fa-level-down"></i></div> -->
                <small>&euro; {{number_format($total * $btcToEuroPrice, 2)}}</small>
	    </div>
	</div>
    </div>
</div>
