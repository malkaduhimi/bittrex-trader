<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Autotrade\AutotradeAction;

class TradeCoins extends Command {
    
    // minimum amount to leave on account after trading
    const MIN_BITCOIN_TO_LEAVE = 0.01;
    const AMOUNT_TO_AUTOTRADE = 0.005;
    
    protected $bittrex = null;
    
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'bittrex:trade';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Does the automatic trading of the coins.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $this->bittrex = App::make('bittrex');

        $this->processAutoTradeActions();
        
        $this->newAutoTradeActions();
	}

    public function processAutoTradeActions()
    {
        $this->info("---- Now start updating the earlier buy trades");
        // update the status of our buy auto trades
        $buyAutoTrades = AutotradeAction::where('status', AutotradeAction::STATUS_START)->get();
        
        foreach($buyAutoTrades as $buyAutoTrade) {
            if ($this->isOrderClosed($buyAutoTrade->buyOrderId)) {
                $this->makeSellOrder($buyAutoTrade);
            } elseif($this->isOrderOld($buyAutoTrade->buyOrderId)) {
                $this->cancelOrder($buyAutoTrade);
            }
        }
        
        $this->info("---- Now start updating the earlier sell trades");
        // update the status of our sell auto trades
        $sellAutoTrades = AutotradeAction::where('status', AutotradeAction::STATUS_BOUGHT)->get();
        foreach($sellAutoTrades as $sellAutoTrade) {
            if ($this->isOrderClosed($sellAutoTrade->sellOrderId)) {
                $sellAutoTrade->status = AutotradeAction::STATUS_SOLD;
                $sellAutoTrade->profit = $this->getProfit($sellAutoTrade);
                $sellAutoTrade->save();
            }
        }
    }
    
    private function getProfit($sellAutoTrade)
    {
        $profit = 0;
        $buyResponse  = $this->bittrex->getOrder($sellAutoTrade->buyOrder);
        $sellResponse = $this->bittrex->getOrder($sellAutoTrade->sellOrder);
        
        if ( ! empty($buyResponse['success']) && ! empty($sellResponse['success'])) {
            $costs  = $buyResponse['result']['CommissionPaid'] + $buyResponse['result']['Price'];
            $income = $sellResponse['result']['Price'] - $sellResponse['result']['CommissionPaid'];
            
            $profit = $income - $costs;
        }
        
        return $profit;
    }
    
    private function isOrderOld($orderId)
    {
        $old = false;
        
        $response = $this->bittrex->getOrder($orderId);
        
        if ( ! empty($response['success'])) {
            $old = strtotime($response['result']['Opened']) < strtotime('30 minutes ago');
        }
        
        return $old;
    }

    private function cancelOrder($autoTrade)
    {
        $response = $this->bittrex->cancel($autoTrade->buyOrderId);
        
        if ( ! empty($response['success'])) {
            $autoTrade->status = AutotradeAction::STATUS_CANCELLED;
            $autoTrade->save();
            
            return true;
        } else {
            return false;
        }
    }
    
    private function makeSellOrder($autoTradeAction)
    {
        $market = Market::where('currency', $autoTradeAction->currency)->first();
        $price  = $this->getSellPrice($market);
        
        $this->info("Going to sell from {$market->name}, amount: {$autoTradeAction->amount} for $price per piece");
        $sellResponse = $this->bittrex->sellLimit($market->name, $autoTradeAction->amount, $price);
        
        if ( ! empty($sellResponse['success'])) {
            $autoTradeAction->status = AutotradeAction::STATUS_BOUGHT;
        
            $orderId = $sellResponse['result']['uuid'];
            
            $autoTradeAction->sellOrderId = $orderId;
            $autoTradeAction->sellPrice   = $price;
            
            $autoTradeAction->save();
        } else {
            $this->error("Selling of {$market->name} failed");
            $this->error(print_r($sellResponse, true));
        }
    }
    
    public function newAutoTradeActions()
    {
        $markets = Market::all();
        
        $i = 0;

        $balance = $this->getAvailableBalance();
        $this->info("---- Now start buying, BTC balance is $balance");
        
        while ($i < $markets->count() && $balance > (self::MIN_BITCOIN_TO_LEAVE + self::AMOUNT_TO_AUTOTRADE)) {
            
            $market = $markets->get($i);
            
            if (
                $market->autoTrade && ! $this->isAlreadyAutotrading($market) 
                && $this->isMarketInteresting($market) && ($price = $this->getBuyPrice($market)) > 0
            ) {
                
                $amount = $this->getBuyAmount($price);
                
                $this->info("Market {$market->name} is interesting, going to buy $amount for $price per piece");
                
                $buyResponse = $this->bittrex->buyLimit($market->name, $amount, $price);
                
                if ( ! empty($buyResponse['success'])) {
                    $orderId = $buyResponse['result']['uuid'];
                    $this->makeAutotradeAction($market, $amount, $price, $orderId);
                } else {
                    $this->error("Buying failed");
                }
                
                $balance = $this->getAvailableBalance();
            }
            
            $i++;
        }

    }
    
    // TODO tweak
    protected function isMarketInteresting($market)
    {
        $interesting = false;
        $marketHistoryResponse = $this->bittrex->getMarketHistory($market->name);
        if ( ! empty($marketHistoryResponse['success']) && ! empty($marketHistoryResponse['result'])) {
            // check if there is a stable increase in the last 5 minutes.
            $trades = array_filter(
                                   $marketHistoryResponse['result'],
                                   function($trade) {
                                       return (strtotime($trade['TimeStamp']) > strtotime('5 minutes ago'));
                                   }
                                   );
            
            $maxPrice = max(
                            array_map(
                                      function($trade) {
                                          return floatval($trade['Price']);
                                      },
                                      $trades
                                      )
                            );
            
            $interesting = (
                            count($trades) > 20 // number of trades should be higher than 4 per minute...
                            && $trades[0]['Price'] > $trades[count($trades) - 1]['Price'] // last price is higher than the price 5 mins ago.
                            && $maxPrice >= $trades[0]['Price'] * 1.006 // percentage should be 0.6% or higher (transaction costs)
                            );
            
            //$this->info('market ' . $market->name . ($interesting ? ' is interesting ' : ' is NOT interesting'));
        } else {
            $this->error("Failed to get market history for {$market->name}");
        }
        
        
        return $interesting;
    }
    
    // TODO tweak
    private function getSellPrice($market)
    {
        $marketHistoryResponse = $this->bittrex->getMarketHistory($market->name);
        
        if ( ! empty($marketHistoryResponse['success']) && count($marketHistoryResponse['result']) > 0) {

            $maxPrice = max(
                            array_map(
                                      function($trade) {
                                          return (strtotime($trade['TimeStamp']) > strtotime('5 minutes ago')) ? floatval($trade['Price']) : 0;
                                      },
                                      $marketHistoryResponse['result']
                                      )
                            );
            
            // if last trade was max price in last 5 minutes, sell for 1% higher.
            if($marketHistoryResponse['result'][0]['Price'] == $maxPrice) {
                return $maxPrice * 1.01;
            } else {
                // else, sell for max price in last 5 minutes.
                return $maxPrice;
            }
            
        } else {
            $this->error("Failed to get market history for {$market->name}");
        }
        
        
        return $interesting;
    }
    
    private function isOrderClosed($orderId) {
        $closed = false;
        
        $orderResponse = $this->bittrex->getOrder($orderId);
        
        if ( ! empty($orderResponse['success'])) {
            $closed = ! $orderResponse['result']['IsOpen'];
        } else {
            $this->error("Failed to fetch status of order $orderId");
        }
        
        return $closed;
    }
    
    protected function getBuyPrice($market)
    {
        $tickerResponse = $this->bittrex->getTicker($market->name);
        
        if ( ! empty($tickerResponse['success']) && $tickerResponse['result']['Bid'] < 1) {
            return $tickerResponse['result']['Bid'];
        } else {
            $this->error("Couldn't get bid price");
            return 0;
        }
    }
    
    protected function getBuyAmount($price)
    {
        return min([$this->getAvailableBalance() / 2, self::AMOUNT_TO_AUTOTRADE]) / $price;
    }

    protected function isAlreadyAutotrading($market)
    {
        return AutotradeAction::where('currency', $market->currency)
            ->where(function($q) {
                    $q->where('status', AutotradeAction::STATUS_START)
                        ->orWhere('status', AutotradeAction::STATUS_BOUGHT);
                })
            ->count() > 0;
    }
        
    private function makeAutotradeAction($market, $amount, $price, $orderId)
    {
        $autotradeAction = new AutotradeAction;
        $autotradeAction->buyOrderId  = $orderId;
        $autotradeAction->currency    = $market->currency;
        $autotradeAction->amount      = $amount;
        $autotradeAction->buyPrice    = $price;
        $autotradeAction->status      = AutotradeAction::STATUS_START;
        
        $autotradeAction->save();
    }

    private function getAvailableBalance()
    {
        $response = $this->bittrex->getBalance('BTC');
        
        if( ! empty($response['success'])) {
            return $response['result']['Available'];
        } else {
            $this->error("Failed getting btc balance");
            $this->error(print_r($response, true));
            
            return 0;
        }
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
