<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateCoins extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'bittrex:update-coins';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Updates the coin database from bittrex.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $bittrex = new Bittrex\Bittrex(Settings::get('key'), Settings::get('secret'));
        $marketResponse = $bittrex->getMarkets();
        
        if ($marketResponse['success']) {
            $markets = $marketResponse['result'];
            
            foreach($markets as $marketInfo) {
                if ($marketInfo['BaseCurrency'] == 'BTC') {
                    $market = Market::firstOrNew(['name' => $marketInfo['MarketCurrency']]);
                    $market->name     = $marketInfo['MarketName'];
                    $market->currency = $marketInfo['MarketCurrency'];
                    $market->active   = $marketInfo['IsActive'];


                    $this->info("updating {$market->name}");
                    $market->save();
                }
            }    
        } else {
            $this->error("Couldn't fetch markets from bittrex");
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
        return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
        return [];
	}

}
