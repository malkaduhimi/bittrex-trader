<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Statistics\StatisticsUpdater;

class UpdateStats extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'bittrex:update-stats';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update the statistics of bittrex.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $statsUpdater = new StatisticsUpdater;
        
        $statsUpdater->updateMarketPrices();
        
        $statsUpdater->updateAutotradeStatus();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
