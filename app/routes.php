<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/*************************************
 * Admin
 */

$admin = Clearworks::getEnvironment('admin');

// add the show file page...
$admin->addPage(new \Clearweb\Clearwebapps\File\ShowFilePage);

$admin->addPage(new Autotrade\AutotradePage, true);

$admin->addPage(new Coin\CoinsPage);

// set the settings page as the default page for the admin
$admin->addPage(new \Clearweb\Clearwebapps\Setting\SettingsPage);

// populate the menu
Event::listen('env.init', function() {
        
    Menu::addLink(trans('Autotrades'), Clearworks::getPageUrl(new Autotrade\AutotradePage, ['only-open' => true]), Clearworks::getPageUrl(new Autotrade\AutotradePage));
    
    Menu::addLink(trans('Coins'), Clearworks::getPageUrl(new Coin\CoinsPage));

    Menu::addLink(trans('clearwebapps::settings.settings'), Clearworks::getPageUrl(new \Clearweb\Clearwebapps\Setting\SettingsPage));

    Settings::addPackage(new Settings\BittrexSettings);
});
